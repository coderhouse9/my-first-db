const { options } = require('./options/SQLite3.js');
const knex = require('knex')(options);

knex.from('pokes').where('price', '7000').update({ price: 8900 })
	.then(() => console.log('Poke updated'))
	.catch(err => { console.log(err); throw err })
	.finally(() => knex.destroy())