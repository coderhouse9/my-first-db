const { options } = require('./options/SQLite3.js');
const knex = require('knex')(options);

knex.from('pokes').del()
	.then(() => console.log('All pokes deleted'))
	.catch(err => { console.log(err); throw err })
	.finally(() => knex.destroy())