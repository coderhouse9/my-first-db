const { options } = require('./options/SQLite3.js');
const knex = require('knex')(options);
const pokes = require('./insert_pokes.js');

(async () => {
	try {
		console.log('--> We erase all pokes');
		await knex('pokes').del()

		console.log('--> We insert pokes');
		await knex('pokes').insert(pokes)

		console.log('--> We read all pokes');
		let rows = await knex.from('pokes').select('*')
		for (row of rows) console.log(`${row['id']} ${row['name']} ${row['price']}`);

		console.log('--> Insert one more  poke');
		await knex('pokes').insert({ name: 'Pikachu', price: 4321 })

		console.log('--> We read the updated pokes');
		rows = await knex.from('pokes').select('*')
		for (row of rows) console.log(`${row['id']} ${row['name']} ${row['price']}`);
	}
	catch (err) {
		console.log(err);
	}
	finally {
		knex.destroy();
	}
})()