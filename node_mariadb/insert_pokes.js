const { options } = require('./options/mariaDB.js');
const knex = require('knex')(options)

const pokes = [
	{ name: 'Volcarona', price: 5264 },
	{ name: 'Golisopod', price: 7127 },
	{ name: 'Pangoro', price: 2000 },
	{ name: 'Glastrier', price: 4050 },
	{ name: 'Honchkrow', price: 3500 },
	{ name: 'Purugl', price: 9380 },
	{ name: 'Corviknight', price: 6289 },
	{ name: 'Stantler', price: 4291 }
]

knex('pokes').insert(pokes)
	.then(() => console.log('Data inserted'))
	.catch(err => { console.log(err); throw err })
	.finally(() => knex.destroy())

module.exports = pokes;